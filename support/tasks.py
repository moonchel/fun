from __future__ import absolute_import

from fun.celery import app

from django.contrib.auth.models import User
from django.conf import settings


@app.task
def tokens_update():
    apis = settings.CONNECTIONS 
    users = User.objects.all()
    wapis = {}

    for ctype, module in apis.iteritems():
        module, api_class = self.apis[ctype].rsplit('.', 1)
        api = getattr(import_module(module), api_class)(ctype)
        wapis.update({ctype : api})

    for user in users:
        token = user.token_set.all().first()
        if token:
            wapis[ctype].refresh_token(token)