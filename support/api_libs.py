from django.shortcuts import redirect
from urlparse import urljoin
from django.core.urlresolvers import reverse
from widgets.models import Token
import requests
import base64
import json 
from urllib import urlencode

class MainApi(object):

    def __init__(self, ctype, request=None):
        if request:
            self.request = request
            self.user = request.user
            self.session_key = request.session.session_key
        self.redirect_uri = reverse('home:add_connection', args=(ctype,))

class ApiToken(object):
    def __init__(self, **entries): 
        self.__dict__.update(entries)

class SpotifyApi(MainApi):
    client_id = '66723b975c1348ac8fa93e30b1c181fc'
    client_secret = 'd20d705b917a447297bf5a972f75ebcb'
    scope = [
        'playlist-read-private', 'playlist-read-collaborative',
        'playlist-modify-public'
    ]
    auth_url = 'https://accounts.spotify.com/authorize/?'
    auth_params = {
        'client_id' : '',
        'scope' : '',
        'redirect_uri' : '',
        'response_type' : 'code',
        'state' : ''
    }
    token_url = 'https://accounts.spotify.com/api/token'
    token_params = {
        'grant_type' : 'authorization_code',
        'code' : '',
        'redirect_uri' : '',
    }
    session_key = ''
    playlists_url = 'https://api.spotify.com/v1/me/playlists'

    def authorize_url(self):
        self.auth_params['client_id'] = self.client_id
        self.auth_params['scope'] = ' '.join(self.scope)
        self.auth_params['redirect_uri'] = self.request.build_absolute_uri(self.redirect_uri)
        self.auth_params['state'] = self.session_key
        return self.auth_url + urlencode(self.auth_params) 

    def exchange_token(self, code):
        self.token_params['redirect_uri'] = self.request.build_absolute_uri(self.redirect_uri)
        self.token_params['code'] = code
        headers = {'Authorization' : 'Basic %s' % base64.b64encode('%s:%s' % (self.client_id, self.client_secret))}
        page = requests.post(self.token_url, data=self.token_params, headers=headers)
        # access_token token_type scope expires_in refresh_token in page.content
        response = json.loads(page.content)
        return ApiToken(**response)

    def refresh_token(self, token):
        data = {
            'grant_type' : 'refresh_token',
            'refresh_token' : token.refresh_token
        }
        headers = {'Authorization' : 'Basic %s' % base64.b64encode('%s:%s' % (self.client_id, self.client_secret))}
        page = requests.post(self.token_url, data=self.token_params, headers=headers)
        response = json.loads(page.content)
        # save new token
        token.access_token = response.access_token
        token.expires_in = response.expires_in
        token.refresh_token = response.refresh_token
        token.save()
        return token

    def get_my_playlists(self, token):
        print token
        headers = {'Authorization' : 'Bearer %s' % token}
        playlists_page = requests.get(self.playlists_url, headers=headers)
        response = json.loads(playlists_page.content)
        return response

import soundcloud

class SoundCloudApi(MainApi):
    client_id = '9a53a63f269908e0c9705eb355790c34'
    client_secret = '64ed0d500844e65f2c14d62ae40906c6'
    
    def __init__(self, ctype, request=None, with_token=False):
        super(SoundCloudApi, self).__init__(ctype, request=request)
        if with_token:
            token = request.user.token_set.get(name='soundcloud').actual_token
            self.client = soundcloud.Client(access_token=token)
        else:    
            self.client = soundcloud.Client(
                client_id=self.client_id,
                client_secret=self.client_secret,
                redirect_uri=self.request.build_absolute_uri(self.redirect_uri),
            )
 
    def authorize_url(self):
        return self.client.authorize_url()

    def exchange_token(self, code):
        return self.client.exchange_token(code)

    def refresh_token(self, token):
        if '*' in token.scope or 'non-expiring' in token.scope:
            return
        self.client = Soundcloud.new(
            client_id=self.client_id,
            client_secret=self.client_secret,
            refresh_token=token.refresh_token
        )
        # save new token
        rtoken = self.client.access_token
        token.access_token = rtoken.access_token
        token.expires_in = rtoken.expires_in
        token.refresh_token = rtoken.refresh_token
        token.save()
        return token

    def get(self, url):
        return self.client.get(url)