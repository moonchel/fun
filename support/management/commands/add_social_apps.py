from django.core.management.base import BaseCommand, CommandError
from django.contrib.sites.models import Site
from django.conf import settings
from support.functions import get_or_none
from allauth.socialaccount.models import SocialApp


class Command(BaseCommand):
    help = 'Adding social apps to db'

    def handle(self, *args, **options):
        csite, created = Site.objects.get_or_create(pk=settings.SITE_ID)

        if created:
            if settings.DEBUG:
                csite.domain = 'local.host'
                csite.name = 'local.host'
            else:
                csite.domain = 'deploy.host'
                csite.name = 'deploy.host'

        csite.save()
        fb = SocialApp.objects.create(
            provider = 'facebook',
            name = 'facebook',
            client_id = settings.FACEBOOK_APP_ID,
            secret = settings.FACEBOOK_APP_SECRET
        )
        google = SocialApp.objects.create(
            provider = 'google',
            name = 'google',
            client_id = settings.GOOGLE_CLIENT_ID,
            secret = settings.GOOGLE_CLIENT_SECRET
        )
        fb.sites.add(csite)
        google.sites.add(csite)