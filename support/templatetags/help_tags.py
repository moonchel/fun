from django import template
from django.template import Library

register = template.Library()

@register.filter
def cool_name(name):
    return name.capitalize().replace('_', ' ')