$(document).ready(function(){
    var options = {
        cellHeight: 80,
        verticalMargin: 10
    };

    $('.grid-stack').gridstack(options);

    if ($('#editor').length) {
        CKEDITOR.replace('editor');
    }

    if ($('#soundcloud').length) {
        var swidget = SC.Widget($('#soundcloud')[0]);
    }

    $('input[type="date"]').datepicker({ dateFormat: 'dd-mm-yy'});

    $('.todo_form').on('submit', function(event){
        event.preventDefault();
        var count = $('.todo_list').find('span').html();
        task = {};
        var form = $(this);
        task['text'] = form.find('#id_task').val();
        task['date'] = form.find('#id_when').val();
        $.ajax({
            url : form.attr('action'),
            data : JSON.stringify(task),
            type: "POST",
            contentType: "application/json",
        }).done(function(data){
            form[0].reset();
            $('.todo_list').append('<li>' + task['date'] + ' ' + task['text'] + '</li>');
        });
    });
});