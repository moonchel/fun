from django.shortcuts import render
from django.views.generic.base import TemplateView, RedirectView
from widgets.widgets_lib import WidgetsControl  
from django.contrib.auth.decorators import login_required
from django.conf import settings
from support.api_libs import SpotifyApi
from django.core.urlresolvers import reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from datetime import timedelta, datetime

from widgets.models import Token
from importlib import import_module


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "home/home.html"

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['widgets'] = WidgetsControl(self.request).get_context()
        return context

class ConnectApiView(LoginRequiredMixin, TemplateView):
    template_name = 'home/connections.html'

    def get_context_data(self, **kwargs):
        context = super(ConnectApiView, self).get_context_data(**kwargs)
        context['connections'] = {}
        connections = self.request.user.token_set.values_list('name', flat=True)
        for x in settings.CONNECTIONS.keys():
            context['connections'][x] = 'checked' if x in connections else ''
        return context

class WidgetsView(LoginRequiredMixin, TemplateView):
    template_name = 'home/widgets.html'

    def get_context_data(self, **kwargs):
        context = super(WidgetsView, self).get_context_data(**kwargs)
        context['widgets'] = {}
        widgets = self.request.user.widget_set.filter(active=True).values_list('name', flat=True)
        for x in settings.WIDGETS.keys():
            context['widgets'][x] = 'checked' if x in widgets else ''
        return context

class AddConnection(RedirectView):
    apis = settings.CONNECTIONS 

    def get_redirect_url(self, *args, **kwargs):
        ctype = kwargs['ctype']
        module, api_class = self.apis[ctype].rsplit('.', 1)
        api = getattr(import_module(module), api_class)(ctype, request=self.request)
        if self.request.GET:
            state = self.request.GET.get('state', '')
            code = self.request.GET.get('code', '')
            widget, created = self.request.user.widget_set.get_or_create(
                name=ctype,
                user=self.request.user,
            )
            rtoken = api.exchange_token(code)
            if not getattr(rtoken, 'error', None):
                token, created = Token.objects.update_or_create(
                    user=self.request.user,
                    actual_token=rtoken.access_token,
                    name=ctype,
                    widget=widget,
                    scope='|'.join(rtoken.scope) if isinstance(rtoken.scope, list) else rtoken.scope
                )
                expires_in = getattr(rtoken, 'expires_in', None)
                refresh_token = getattr(rtoken, 'refresh_token', None) 
                if refresh_token and expires_in:
                    token.refresh_token = refresh_token
                    token.expires_in = datetime.now() + timedelta(seconds=int(expires_in))
                    token.save()
                return reverse('home:connections')
        return api.authorize_url() 

@login_required
def photo_save(request):
    photo = Photo(user=request.user, photo=request.FILES['file'])
    photo.save()
    return HttpResponse('Uploaded')