from django.conf.urls import url

from views import (
	HomeView, ConnectApiView, 
	WidgetsView, AddConnection
)

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^widgets/$', WidgetsView.as_view(), name='widgets'),
    url(r'^connections/$', ConnectApiView.as_view(), name='connections'),
    url(r'^connect/(?P<ctype>\w+)/$', AddConnection.as_view(), name='add_connection'),
]