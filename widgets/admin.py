from django.contrib import admin
from models import (
	Widget, Task, Image, Token
)

class TaskAdmin(admin.ModelAdmin):
    pass

class WidgetAdmin(admin.ModelAdmin):
    pass

class ImageAdmin(admin.ModelAdmin):
    pass

class TokenAdmin(admin.ModelAdmin):
    pass
    
admin.site.register(Widget, WidgetAdmin)
admin.site.register(Task, TaskAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Token, TokenAdmin)
