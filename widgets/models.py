from __future__ import unicode_literals

from django.db import models
from datetime import datetime

from django.contrib.auth.models import User
from imagekit.models import ProcessedImageField
from django.conf import settings

CONNECTIONS = (
    (('%s' % x, '%s' % x) for x in settings.CONNECTIONS)
)

WIDGETS = (
    (('%s' % x, '%s' % x) for x in settings.WIDGETS.keys())
)

class Widget(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(choices=WIDGETS, max_length=30)
    x = models.IntegerField(default=0)
    y = models.IntegerField(default=0)
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    active = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Widget"
        verbose_name_plural = "Widgets"

    def __unicode__(self):
        return '%s %s' % (self.user, self.name)

class Task(models.Model):
    user = models.ForeignKey(User, null=True)
    text = models.CharField(max_length=50)
    date = models.DateField(null=True)
    
    class Meta:
        verbose_name = "Task"
        verbose_name_plural = "Tasks"

    def __unicode__(self):
        return self.text

class Image(models.Model):
    user = models.ForeignKey(User)
    image = ProcessedImageField(upload_to='images/', format='JPEG', options={'quality': 60})

    def __unicode__(self):
        return '%s %s' % (self.user, self.image)


class Token(models.Model):
    name = models.CharField(max_length=64, choices=CONNECTIONS)
    user = models.ForeignKey(User)
    widget = models.OneToOneField(Widget)
    actual_token = models.TextField()
    refresh_token = models.TextField(null=True)
    expires_in = models.DateTimeField(null=True)
    scope = models.TextField()
    
    class Meta:
        verbose_name = "Token"
        verbose_name_plural = "Tokens"

    def __unicode__(self):
        return '%s %s' % (self.user, self.name)

    def expires(self):
        if not self.expires_in:
            return False
        return True if datetime.now() >= self.expires_in else False