from models import Task
from django.template import Context
from django.template.loader import get_template
from django.contrib.gis.geoip import GeoIP
import requests
import json
from django.conf import settings
from importlib import import_module
from support.api_libs import SpotifyApi, SoundCloudApi
from urllib import quote


class MainWidget():
    error_template_name = 'widgets/error.html'
    context_error = False

    def __init__(self, request, widget):
        self.widget = widget
        self.request = request
        context = self.get_context()
        template = get_template(
            self.error_template_name if self.context_error else self.template_name
        )
        self.html = None if context is None else template.render(Context(context))

    def get_context(self):
        return {}

    def check_token(self):
        context = {}
        self.token = self.request.session.get('%s_%s_token' % (self.request.user.email, self.widget.name), None)
        try:
            self.token = self.request.session.get(
                '%s_%s_token' % (self.request.user.email, self.widget.name), 
                self.request.user.token_set.get(widget=self.widget)
            )
        except Exception, e:
            self.context_error = True
            context['error'] = 'Please, create new connection with %s' % self.widget.name
            return context

        if self.token.expires():
            self.token = self.api.refresh_token()
            self.request.session['%s_%s_token' % (
                self.request.user.email, self.widget.name)] = self.token.access_token

        return context

class TextEditorWidget(MainWidget):
    template_name = 'widgets/text_editor.html'

class PhotoGallery(MainWidget):
    template_name = 'widgets/photo_gallery.html'

    def get_context(self):
        images = request.user.image_set.all()
        return {'images' : images}

class TodoList(MainWidget):
    template_name = 'widgets/todo.html'

    def get_context(self):
        tasks = request.user.task_set.all()
        return {'todo_list' : tasks}

class Weather(MainWidget):
    template_name = 'widgets/weather.html'
    weather_key = 'f382bd5830e051649628ef1649fa3716'
    img_weather_url = 'http://openweathermap.org/img/w/'
    url = 'http://api.openweathermap.org/data/2.5/weather'

    def get_context(self):
        g = GeoIP()
        ip = request.META.get('REMOTE_ADDR', None)
        context = {}
        if ip:
            city = g.city(ip)
            if city:
                context = Context({'city' : city})
                params = {
                    'lat' : city['latitude'],
                    'lon' : city['longitude'],
                    'appid' : self.weather_key,
                }
                page = self.requests.get(self.url, params)
                context['weather'] = json.loads(page.content)
                context['img_weather_url'] = self.img_weather_url
            else:
                return None
        else:
            return None
        return context

class Spotify(MainWidget):
    template_name = 'widgets/spotify.html'

    def get_context(self):
        self.api = SpotifyApi('spotify', self.request)
        context = self.check_token()
        if self.context_error:
            return context
        # context['playlists'] = self.api.get_my_playlists(self.token)
        # context['uris'] = [quote(x['uri']) for x in context['playlists']['items']]
        return context

class SoundCloud(MainWidget):
    template_name = 'widgets/soundcloud.html'

    def get_context(self):
        self.api = SoundCloudApi('soundcloud', self.request, with_token=True)
        context = self.check_token()
        if self.context_error:
            return context
        from pprint import pprint
        import json
        collections = json.loads(self.api.get('me/activities').raw_data)['collection']
        
        for c in collections:
            if c['type'] == 'playlist-repost':
                context['playlist'] = c['origin']['uri']
                break

        return context

class WidgetsControl():
    route = {
        widget : widget_class for (widget, widget_class) in settings.WIDGETS.iteritems()
    }

    def __init__(self, request):
        widgets = request.user.widget_set.filter(active=True)
        self.context = {}
        for widget in widgets:
            module, widget_class = self.route[widget.name].rsplit('.', 1)
            self.context.update({
                widget : getattr(import_module(module), widget_class)(request, widget)
            })

    def get_context(self):
        return self.context