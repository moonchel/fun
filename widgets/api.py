from tastypie.resources import ModelResource
from tastypie.authorization import DjangoAuthorization

from models import Task


class TaskResource(ModelResource):
    class Meta:
        queryset = Task.objects.all()
        resource_name = 'task'
        list_allowed_methods = ['get', 'post', 'put']
        authorization = DjangoAuthorization()
        always_return_data = True