from django.conf.urls import url, include
from views import SpotifyView, ConnectApiView

from api import TaskResource

from tastypie.api import Api
v1_api = Api(api_name='v1')

v1_api.register(TaskResource())

urlpatterns = [
    url(r'^api/', include(v1_api.urls)),
]